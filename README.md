# React LostInTranslation

A translation app that transform given words in sign language so you can learn how to speak with only you're hands. Log in to the app by using you're name as username and nothing is further required to create an account.

# Installation & start-up

Fork to project to you're own git repository or download the zip file. From there open the terminal in the root folder (ctr + shift + `) and type:<br>
'cd react-translation' in the terminal<br>
Then type:<br>
npm i<br>
This will install all dependencies for the project. To start the program type:<br>
npm start<br>
Always be sure to be in the react-translation folder before typing npm start, else the program won't work.

# Usage
The first screen you will see is this:
![alt text](/react-translation/public/readmeImages/LoginScreen.PNG?raw=true)<br>
In this screen you can fill in you're username. This is required and has a minimum lenght of 3. This is all that is required for creating a account.<br>
The next screen you will see is the translation screen:
![alt text](/react-translation/public/readmeImages/TranslationScreen.PNG?raw=true)<br>
In this screen you can type what you want to translated to sign language in the inputfield. In this example this translated words are: Hello world.<br>
Next to you're account name is a person icon. This is a button to open a pop-up that shows two buttons. A button to go to the profile screen and a button to log out.
![alt text](/react-translation/public/readmeImages/ProfileScreen.PNG?raw=true)<br>
In the profile page you can see the history of the last 10 translations you made. You can also delete you're translation history here, you can log out of the program here and you can go back to the translation page.
# Colleberators

Yolothan: https://gitlab.com/Yolothan
