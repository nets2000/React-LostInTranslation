import '../src/Style/Persistent.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Startup from './views/Startup';
import Translation from './views/Translation.jsx';
import Profile from './views/Profile';
import DataManager from './components/Managers/DataManager';


function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Routes>
          <Route path="/" element={ <Startup />} />
          <Route path={"/" + DataManager.instance.getTranslationPage() } element={ <Translation />} />
          <Route path={"/" + DataManager.instance.getProfilePage() }  element={ <Profile />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
