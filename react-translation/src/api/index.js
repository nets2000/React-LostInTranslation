const apiKey = process.env.REACT_APP_API_KEY;

// Create headers for the headers field for connection with API
export const createHeaders = () => {
    return {
        'Content-Type': 'application/json',
        'x-api-key': apiKey
    }
}