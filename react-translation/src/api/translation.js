import { createHeaders } from ".";

const apiUrl = process.env.REACT_APP_API_URL;

// Update the translation history by adding the new message to the translation list
export const addTranslationToHistory = async (user, message) => {
    try {
        const response = await fetch(`${apiUrl}/${user.id}`, {
            method: "PATCH",
            headers: createHeaders(),
            body: JSON.stringify( {
                translations: [ ...user.translations, message]
            })
        });

        if (!response.ok) {
            throw new Error("Translations could not be updated");
        }

        const result = await response.json();
        return[ null, result ];
    } catch (error) {
        return [error.message, null];
    }
}

// Clear the translation history by emptying the translation list by setting it to an empty array
export const clearTranslationHistory = async (user) =>{
    try {
        const response = await fetch(`${apiUrl}/${user.id}`, {
            method: "PATCH",
            headers: createHeaders(),
            body: JSON.stringify( {
                translations: []
            })
        });

        if (!response.ok) {
            throw new Error("Translations could not be updated");
        }

        const result = await response.json();
        return[ null, result ];
    } catch (error) {
        return [error.message, null];
    }
}