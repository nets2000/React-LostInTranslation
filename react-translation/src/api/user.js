import  { createHeaders } from "./index";

const apiUrl = process.env.REACT_APP_API_URL

// Check if the user exists in the API
const checkForUser = async (username = "") => {
    try {
        // Check if the user exists based on username
        const response = await fetch(apiUrl + "?username=" + username);
        if (!response.ok) {
            throw new Error("Could not get response from website");
        }
        // transform data to json and return the data
        const data = await response.json();
        return [null, data];
    } catch (error) {
        return [error.message, []];
    }
}

// Create user in API
const createUser = async (username) => {
    try {
        // Users can be created with just the api url
        const response = await fetch(apiUrl, {
            method: "POST",
            headers: createHeaders(),
            body: JSON.stringify( {
                username,
                translations: []
            })
        });

        if (!response.ok) {
            throw new Error("Could not create new user with username: " + username);
        }
        // Return data of newly created user
        const data = await response.json();
        return [null, data];
    } catch (error) {
        return [error.message, []];
    }
}

// Login the user in the program
export const loginUser = async (username) => {
    // Check if user exists in API
    const [checkError, user] = await checkForUser(username);

    // Return error if the one is given
    if (checkError !== null) {
        return [checkError, null]
    }

    // If user check gave back a user return the user
    if (user.length > 0) {
        return [null, user.pop()];
    }

    // Return a newly created user if user didn't exist yet
    return await createUser(username);
}