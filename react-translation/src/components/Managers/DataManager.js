import { loginUser } from "../../api/user.js";
import Singleton from "../../utils/singleton.js";
import { storageRead, checkIfUserExists } from "../../utils/storage.js";

// Go to singleton class for explanation of singleton usage
// A singleton class that is accessible from all scripts to store and handout data from

// Base variables that stay the same for all users and need to be used globally
const translationPage = "translation", profilePage = "profile";
const savedUserDataName = "translation-user";

export default class DataManager extends Singleton {
  // Set the user object
  setUser(user) {
    this.user = user;
  }

  // Get username of user if user exists
  getUsername() {
    if (!this.user) { return; }
    return this.user.username;
  }

  // Set translations field to be an empty array if user exists
  emptyTranslations() {
    if (!this.user) { return; }
    this.user.translations = [];
  }

  // Add translation to translations array if user exists
  addTranslation(message) {
    if (!this.user) { return; }
    this.user.translations.push(message);
  }

  // Get translations array if user exists
  getTranslations() {
    if (!this.user) { return; }
    return this.user.translations;
  }

  //Get user id if user exists
  getUserID() {
    if (!this.user) { return; }
    return this.user.userID;
  }

  // Get translation page name
  getTranslationPage() {
    return translationPage;
  }

  // Get profile page name
  getProfilePage() {
    return profilePage;
  }

  // Get key name for local storage
  getSavedUserDataName() {
    return savedUserDataName;
  }

  // Check if the local user exists and set local user if it doesn't exist yet
  checkIfUserExists() {
    if (!this.user && checkIfUserExists()) {
      this.setUser(storageRead(this.getSavedUserDataName()));
      this.setUserOnReloadPage();
    }
  }

  // Set local user data to data in database
  async setUserOnReloadPage() {
    try {
      const [error, user] = await loginUser(this.user.username);
      if (user) {
        this.setUser(user);
      } else {
        throw new Error("Couldn't fetch user. Error message:" + error);
      }
    } catch (error) {
      throw new Error("Couldn't fetch user. Error message:" + error.message);
    }
  }
}
