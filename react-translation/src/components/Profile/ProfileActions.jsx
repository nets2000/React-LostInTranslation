import { useState } from "react";
import { useEffect } from "react"
import { useNavigate } from "react-router-dom";
import { checkIfUserExists, clearStorage } from "../../utils/storage";
import DataManager from "../Managers/DataManager";

const ProfileActions = () => {

    // Set navigate function
    const navigate = useNavigate();

    // Set use state hooks
    const [switchToProfilePage, setSwitchToProfilePage] = useState(false);
    const [loggedOut, setLogout] = useState(false);

    // Functions that need to be done on page update
    useEffect(() => {
        // Check if user exists and if user doesn't exist or if user is logging out sent user back to login page
        if (!checkIfUserExists() || loggedOut) {
            navigate("/", { replace: true });
        }

        // Switch to profile page
        if (switchToProfilePage) {
            navigate("/" + DataManager.instance.getTranslationPage(), { replace: true });
        }
    }, [navigate, switchToProfilePage, loggedOut]);

    // Set switch to profile page to true if go to translation page button is pressed
    const goToTranslationPage = () => {
        setSwitchToProfilePage(true);
    }

    // Clear local storage data and call use effect by calling a hook if log out button is pressed
    const removeLocalData = () => {
        if(!window.confirm("Are you sure you want to log out?")) {
            return;
        }
        
        clearStorage();
        setLogout(true);
    }

    return (
        <>
        <div className="Actions">
            <h1>Actions</h1>
            <button onClick={goToTranslationPage} className="ActionButton1">Translation</button> 
            <button onClick={removeLocalData} className="ActionButton2" >Log out</button> 
        </div>
        </>
    )
}

export default ProfileActions