import DataManager from "../Managers/DataManager"

const ProfileHeader = () => {

    // Show visuals for the header of the profile page
    return (
        <>
        <div className="HeaderProfile">
            <h1 >Profile</h1>
            <p>Welcome to the profile page {DataManager.instance.getUsername()}</p>
        </div>
        </>
    )
}

export default ProfileHeader