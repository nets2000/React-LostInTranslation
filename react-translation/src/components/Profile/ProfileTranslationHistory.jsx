import { useEffect } from "react";
import { useState } from "react";
import { clearTranslationHistory } from "../../api/translation";
import { storageSave } from "../../utils/storage";
import DataManager from "../Managers/DataManager";
import TranslationHistoryListObject from "./TranslationHistoryListObject";

// Set required vars
const maxHistoryElements = 10;
let translationHistoryList = [];
let history = null;

const ProfileTranslationHistory = () => {
    // Set use states
    const [clearHistory, setClearHistory] = useState(false);

    // Update the history tab if the history is edited
    useEffect(() => {
        setHistory();
        
        // Link use effect to a use state hook so this field updates
        if (clearHistory) {
            setClearHistory(false);
        }
    }, [clearHistory]);

    // Function for creating the history list
    const setHistory = () => {
        if (!DataManager.instance.getTranslations()) { return; }
        translationHistoryList = [];
        let counter = 0;

        // Get last 10 elements of list or the entire list if its has less elements
        for (let i = DataManager.instance.getTranslations().length - 1; i>= 0; i--) {
            if (counter < maxHistoryElements) {
                // Add elements to list that shows history objects
                translationHistoryList.push(DataManager.instance.getTranslations()[i]);
                counter++;
            }else {
                // Break loop after 10 rounds
                break;
            }
        }

        // Set the history objects equal to all the data in the translation history list
        history = translationHistoryList.map(
            (translation, index) => <TranslationHistoryListObject key={index + "-" + translation} item={translation}/>
        )
    }
    
    // Clear all history in the program and database when the delete history button is clicked
    const deleteTranslationHistory = async() => {
        if(!window.confirm("Are you sure you want to remove you're translation history?\nThis cannot be undone.")) {
            return;
        }

        try {
            // Clear history in API
            const [clearError] = await clearTranslationHistory(DataManager.instance.user);
            if (clearError !== null) {
                throw new Error(clearError);
            }

            // Call update hook
            setClearHistory(true);
            // Clear local history
            DataManager.instance.emptyTranslations();
            storageSave(DataManager.instance.getSavedUserDataName(), DataManager.instance.user);
        } catch (error) {
            throw new Error(error.message)
        }
    }

    // Set the history for the page load
    setHistory();
    return (
        <>
        <div className="History">
        <h1>History</h1>
            <section>
                
                <ol>
                    {//Show history object
                    history}
                </ol>
            </section>
            <button onClick={deleteTranslationHistory} className="HistoryButton" >Delete history</button> 
            </div>
        </>
    )
}

export default ProfileTranslationHistory