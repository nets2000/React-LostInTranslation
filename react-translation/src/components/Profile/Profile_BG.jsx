import React from "react";
import DataManager from "../Managers/DataManager";

const ProfileBG = () => {

    // Creates upper header and standard background for page
    return (
        <>
            <div className="BgProfileScreen">
                <div className="LogoCloud">
                    <img src="logo/BgCloud.png"alt="Cloud" id="CloudTop"></img>
                    <img src="logo/Logo.png"alt="Logo" id="LogoTop"></img>
                        <div className="ContainerProfilePage">                            
                            <img className="UserPic" src="loginImages/user.png" alt="User"></img>
                            <span id="UserName">{DataManager.instance.getUsername()}</span>
                        </div>
                </div>
                <span className="LoginUpperTitle">Lost in Translation</span>
                <div className="LoginUpperTitleUnderline"></div>
            </div>
        </>
    )
}

export default ProfileBG