const StartupBG = () => {

    // Visual data for login screen
    return (
        <>
            <div className="BgLoginScreen">
                <span className="LoginUpperTitle">Lost in Translation</span>
                <div className="LoginUpperTitleUnderline">
                    <div className="LoginTitleBox">
                        <div className="BgLogoImageLoginScreen" />
                        <span className="LoginMainTitle">Lost in Translation</span>
                        <span className="LoginUnderTitle">Get started</span>
                    </div>
                </div>
            </div>
        </>
    )
}

export default StartupBG