import { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom';
import { loginUser } from '../../api/user.js';
import { checkIfUserExists, storageSave } from '../../utils/storage.js';
import DataManager from '../Managers/DataManager.js';

// Vars that set restriction to username input field and form in general
const usernameConfig = {
    required: true,
    minLength: 3
}

const StartupForm = () => {
    // set use form functions
    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm()

    // Set navigate function
    const navigate = useNavigate()

    // Set use states
    const [loggedIn, setLoggedIn] = useState(false)
    const [loading, setLoading] = useState(false)
    const [apiError, setApiError] = useState(null)

    // Use use effect to send user to translation page if there is already a user logged in or if the user put in a valid username
    useEffect(() => {
        if (checkIfUserExists() || loggedIn) {
            //Set local user
            DataManager.instance.checkIfUserExists();
            navigate(DataManager.instance.getTranslationPage());
        }
    }, [navigate, loggedIn])

    // Log the user in to the program when submit username button is pressed
    const onSubmit = async ({ username }) => {
        setLoading(true);
        // Call login function and set vars that it gives back
        const [error, user] = await loginUser(username)

        // Set api error text
        if (error !== null) {
            setApiError(error);
        }

        // Set user in local data if there is a user is not null
        if (user !== null) {
            storageSave(DataManager.instance.getSavedUserDataName(), user)
            setLoggedIn(true)
        }

        setLoading(false);
    }

    // Set incorrect username message when user tries to submit an invalid username
    const errorMessage = (() => {
        if (!errors.username) {
            return null
        }

        let errorMessage = "";
        switch (errors.username.type) {
            case "required":
                errorMessage = "Name is required";
                break;
            case "minLength":
                errorMessage = `Name is too short (min. ${usernameConfig.minLength} characters)`;
                break;
            default:
                errorMessage = "Unknown error";
                break;
        }

        return <span className='LoginWarnings'>{errorMessage}</span>;
    })()

    return (
        <>
            <form onSubmit={
                // handle submit forwards all data filled in the form to the submit function
                handleSubmit(onSubmit)}>
                <div className="BgLoginFieldDiv">
                    <div className="LoginFieldDiv">
                        <fieldset className='BorderlessObject'>
                            <input className="InputLogin" type='text' placeholder="What's your name?" {// Registers inputs given in the form
                                ...register('username', usernameConfig)} />
                            <button className="LoginButton" type='submit' disabled={
                                // Disable submit button based on loading state
                                loading } />
                            {// Show and update error message of the username input field
                            errorMessage}
                            {// Show logging in text if the program is loading
                            loading && <p className='LoginWarnings'>Logging in...</p>}
                            {// Show api error if there is one
                            apiError && <p className='LoginWarnings'>{apiError}</p>}
                        </fieldset>
                    </div>
                </div>
            </form>
        </>
    )
}

export default StartupForm