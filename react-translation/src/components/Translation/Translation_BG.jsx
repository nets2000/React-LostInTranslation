import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import DataManager from "../Managers/DataManager";
import { clearStorage } from "../../utils/storage";

const TranslationBG = () => {

    const navigate = useNavigate();

    const [hidden, setHidden] = useState(true);
 
    // Sends user to login screen and clears locally saved data
    const LogOut = () =>
    {
        if(!window.confirm("Are you sure you want to log out?")) {
            return;
        }

        navigate("/", { replace: true });
        clearStorage();  
    }

    // Shows or hides popup by changing the hidden var
    const OpenPopUp = () =>
    {
        setHidden(!hidden);
    }

    // Send user to profile page
    const GoToProfile = () =>
    {
        navigate("/" + DataManager.instance.getProfilePage(), { replace: true });  
    }

    return (
        <>
            <div className="BgTranslationScreen">
                <div className="LogoCloud">
                    <img src="logo/BgCloud.png"alt="Cloud" id="CloudTop"></img>
                    <img src="logo/Logo.png"alt="Logo" id="LogoTop"></img>
                        <div className="ContainerProfile">
                            {!hidden ? <div id="PopUpMenuProfile">
                                <img src="loginImages/UpArrow.png" alt="UpArrow" id="UpArrowPopUp"></img>
                                <button className="ProfileButton" onClick={GoToProfile}>Go to Profile</button>
                                <button className="ProfileButton"  onClick={LogOut}>Logout</button>
                            </div>: null}
                            <button className="OpenPopUp" onClick={OpenPopUp}></button>
                            <span id="UserName">{DataManager.instance.getUsername()}</span>
                        </div>
                </div>
                <span className="LoginUpperTitle">Lost in Translation</span>
                <div className="LoginUpperTitleUnderline"></div>
            </div>
        </>
    )
}

export default TranslationBG