import React from "react";
import {useEffect} from 'react';
import { createRoot } from 'react-dom/client';
import { useNavigate } from "react-router-dom";
import { addTranslationToHistory } from "../../api/translation";
import { checkIfUserExists, storageSave } from "../../utils/storage";
import DataManager from "../Managers/DataManager";


const FunctionsTranslate = () => {

    let message = "";
    let bool = true;
    let root = undefined;    
    const navigate = useNavigate();
        
    //This functions makes sure the user can also press the translate button by pressing Enter.
    useEffect(() => {
        if (!checkIfUserExists()) {
            navigate("/", { replace: true });
        }

        const keyDownHandler = event => {          
        //If enter is pressed, it triggers the translateButton() function.
          if (event.key === 'Enter') {
            event.preventDefault();
            translateButton();
          }
        };
    
        document.addEventListener('keydown', keyDownHandler);
    
        return () => {
          document.removeEventListener('keydown', keyDownHandler);
        };
    }, [navigate]);
    
    //This function makes sure the user is unable to input special characters, and limits the 
    //input to a number of 40 characters
    const inputSlice = (event) => {               
       
        event.target.value = event.target.value.replace(/[^a-z A-Z]/g, "")
        if (event.target.value.length > 40) {
            event.target.value = event.target.value.slice(0,40); 
        }
        message = event.target.value;        
    }  


    
    const translateButton = () => {  
        //This makes sure the root is only created once. Otherwise a compiler error appears...
        if(bool)
        {
            bool = false;
            const container = document.querySelector("#image");
            root = createRoot(container);  
        }   
        const allElements = []; 

        if (message !== "") {
            AddTranslationManager(message);
        }
        // It doesn't matter if the user uses capital letters or not. The hand gesture is the same.
        message = message.toLowerCase();
        
        /* the for loop goes through the message and it if the message contains a space, an empty image
        is created to represent that space. If it doesn't, the image with the same letter as the message
        is being created.*/
        for(let i = 0; i < message.length; i++)
        {     
            if(message[i] === " ")
            {
                const space = React.createElement("img", {style: {height:"100px", width: "50px"}, id: "space", key: message[i] + i }, null); 
                allElements.push(space);                
                root.render(allElements) 
               
            }     
            else{          
                const key = React.createElement("img", { src:`hand-signs/${message[i]}.png`, style: {height:"100px", width: "100px"}, key: message[i] + i }, null); 
                allElements.push(key);                
                root.render(allElements);     
            }                                     
             
        }       
    }
    
    // Adds translation to the API and saves the translation locally
    const AddTranslationManager = async (message) => {
        try {
            const [error, result] = await addTranslationToHistory(DataManager.instance.user, message);
            DataManager.instance.addTranslation(message);
            storageSave(DataManager.instance.getSavedUserDataName(), DataManager.instance.user);
        } catch  (error) {
            throw new Error("Couldn't update translations. Error message: " + error.message);
        }
    }
    
    // If the remove translation-button is pressed, it deletes the message and array and resets the render.
    const RemoveTranslation = () =>
    {
        message = "";
        const allElements = [];
        root.render(allElements);
    }

    return (
        <>      
            <input className="InputTranslation" type="text" placeholder="Translate" onInput={inputSlice}></input>       
            <button className="TranslateButton" onClick={translateButton}></button> 
            <span id="BottomColor"><button className="RemoveButton" onClick={RemoveTranslation}>Remove</button></span>  
        </>
    )
};

export default FunctionsTranslate;