const singleton = Symbol('singleton')

/** 
 * A script that is very useful for object oriented programming which you learn in the game development world
 * probably is less useful for react, but it was already completely integrated, when we realized it was too late or big parts had to be rewritten
 * 
 * Creates a static reference to an object so it can be used everywhere
*/

/**
 * This is the super class singleton. Inherit this class if you want to create a singleton
 * You can either use [SUBCLASS_NAME].instance or new [SUBCLASS_NAME], but [SUBCLASS_NAME].instance
 * is preferred, because no new instance is created.
 */
export default class Singleton {
  /**
   * Return the instance of the singleton.
   * @returns {*} Return the instance.
   */
  static get instance () {
    if (!this[singleton]) {
      this[singleton] = new this()
    }

    return this[singleton]
  }

  constructor () {
    const Class = this.constructor

    if (!Class[singleton]) {
      Class[singleton] = this
    }

    return Class[singleton]
  }
}
