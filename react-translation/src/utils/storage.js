import DataManager from "../components/Managers/DataManager.js";

// Save data to the local storage by giving a key and with that key a value can be set of an object with that key
export const storageSave = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value));
}

// Get an item from the local storage based on given key
export const storageRead = (key) => {
    const data = localStorage.getItem(key);

    if (data) {
        return JSON.parse(data);
    }

    return null;
}

// Check if user key exists in local storage and return bool based on user existence
export const checkIfUserExists = () => {
    const item = localStorage.getItem(DataManager.instance.getSavedUserDataName());
    return item !== null ? true : false;
}

// Clear all data from local storage and deletes user from data manager
export const clearStorage = () => {
    localStorage.clear();
    DataManager.instance.setUser(null);
}