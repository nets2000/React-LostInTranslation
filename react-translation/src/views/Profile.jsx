import DataManager from "../components/Managers/DataManager";
import ProfileActions from "../components/Profile/ProfileActions";
import ProfileHeader from "../components/Profile/ProfileHeader";
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory";
import ProfileBG from "../components/Profile/Profile_BG";
import '../Style/ProfilePage.css';

// Set user if user exists, then create page based with the profile objects
const Profile = () => {
    DataManager.instance.checkIfUserExists();
    return (
        <>        
            <ProfileBG/>
            <ProfileHeader />
            <ProfileActions />
            <ProfileTranslationHistory />
        </>
    )
};

export default Profile;