import StartupBG from "../components/Startup/StartupBG";
import '../Style/MainPage.css';
import StartupForm from "../components/Startup/StartupForm";

const Startup = () => {

    // Create page based on start-up objects
    return (
        <>
            <StartupBG />
            <StartupForm />
        </>
    );
};

export default Startup;