import React from "react";
import '../Style/TranslationPage.css';
import TranslationBG from "../components/Translation/Translation_BG";
import FunctionsTranslate from "../components/Translation/Translation_Functions"
import DataManager from "../components/Managers/DataManager";

const Translation = () => {
    DataManager.instance.checkIfUserExists();
    return (
        <>
            <TranslationBG/>        
            <FunctionsTranslate/>                            
            <div className="OutputTranslationDiv" id="image"></div>                              
        </>
    )
};


export default Translation;